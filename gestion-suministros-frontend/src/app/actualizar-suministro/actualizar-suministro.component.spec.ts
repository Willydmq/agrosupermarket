import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarSuministroComponent } from './actualizar-suministro.component';

describe('ActualizarSuministroComponent', () => {
  let component: ActualizarSuministroComponent;
  let fixture: ComponentFixture<ActualizarSuministroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarSuministroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarSuministroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
