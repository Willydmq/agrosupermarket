import swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Suministro } from '../suministro';
import { SuministroService } from '../suministro.service';

@Component({
  selector: 'app-actualizar-suministro',
  templateUrl: './actualizar-suministro.component.html',
  styleUrls: ['./actualizar-suministro.component.css']
})
export class ActualizarSuministroComponent implements OnInit {

  /* *|CURSOR_MARCADOR|* */
  id: number;
  suministro: Suministro = new Suministro();
  constructor(private suministroService: SuministroService, private router: Router, private route: ActivatedRoute) { }

  /**
   * La función ngOnInit() se llama cuando se inicializa el componente
   */
  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.suministroService.obtenerSuministroPorId(this.id).subscribe(dato => {
      this.suministro = dato;
    }, error => console.log(error));
  }

  /**
   * Esta función navega a la página de suministros y muestra un dulce mensaje de alerta.
   */
  irAlaListaDeSuminstros() {
    this.router.navigate(['/suministros']);
    swal('Suministro actualizado', `El suministro ${this.suministro.nombre} ha sido actualizado con exito`, `success`);
  }

  /**
   * La función onSubmit() se llama cuando el usuario hace clic en el botón Enviar. Llama a la función
   * actualizarSuministro() en el archivo suministroService.ts. La función actualizarSuministro() toma
   * dos parámetros: el id del suministro a actualizar y el objeto del suministro. La función
   * actualizarSuministro() devuelve un observable. La función subscribe() se llama en el observable.
   * La función subscribe() toma dos parámetros: una función que se llamará si el observable devuelve
   * un valor y una función que se llamará si el observable devuelve un error. La función a llamar si
   * el observable devuelve un valor se llama dato. La función a llamar si el observable devuelve un
   * error se llama error. La función dato llama a la función irAlaListaDeSuminstros(). El error de
   * función registra el error en la consola.
   */
  onSubmit() {
    this.suministroService.actualizarSuministro(this.id, this.suministro).subscribe(dato => {
      this.irAlaListaDeSuminstros();
    }, error => console.log(error));
  }
}
