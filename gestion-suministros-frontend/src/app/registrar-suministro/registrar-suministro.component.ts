import { Component, OnInit } from '@angular/core';
import { Suministro } from "./../suministro";
import { SuministroService } from '../suministro.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrar-suministro',
  templateUrl: './registrar-suministro.component.html',
  styleUrls: ['./registrar-suministro.component.css']
})
export class RegistrarSuministroComponent implements OnInit {

  suministro: Suministro = new Suministro();

  constructor(private suministroServicio: SuministroService, private router: Router) { }

  ngOnInit(): void {
    console.log(this.suministro);
  }

  /**
   * La función guardarSuministro() se llama cuando el usuario hace clic en el botón "Guardar" en el
   * formulario. La función llama al servicio registrarSuministro() que está definido en el servicio
   * suministro.service.ts. El servicio registrarSuministro() llama a la API registrarSuministro() que
   * está definida en el controlador SuministroController.java. La API registrarSuministro() llama a la
   * función registrarSuministro() que está definida en la clase SuministroService.java. La función
   * registrarSuministro() llama a la función registrarSuministro() que está definida en la clase
   * SuministroRepository.java. La función registrarSuministro() llama a la función save() la cual está
   * definida en la clase JpaRepository.java. La función save() llama a la función save() que está
   * definida en la clase SimpleJpa
   */
  guardarSuministro() {
    this.suministroServicio.registrarSuministro(this.suministro).subscribe(dato => {
      console.log(dato);
      this.irAlaListaDeSuministros();
    }, error => console.log(error))
  }

  /**
   * Navega a la página de suministros
   */
  irAlaListaDeSuministros() {
    this.router.navigate(["/suministros"]);
  }

  /**
   * La función onSubmit() se llama cuando el usuario hace clic en el botón Enviar
   */
  onSubmit() {
    this.guardarSuministro();
    // console.log(this.suministro);
  }

}
