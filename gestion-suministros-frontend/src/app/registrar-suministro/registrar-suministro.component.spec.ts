import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarSuministroComponent } from './registrar-suministro.component';

describe('RegistrarSuministroComponent', () => {
  let component: RegistrarSuministroComponent;
  let fixture: ComponentFixture<RegistrarSuministroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarSuministroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarSuministroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
