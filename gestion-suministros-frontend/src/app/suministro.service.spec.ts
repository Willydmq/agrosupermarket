import { TestBed } from '@angular/core/testing';

import { SuministroService } from './suministro.service';

describe('SuministroService', () => {
  let service: SuministroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SuministroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
