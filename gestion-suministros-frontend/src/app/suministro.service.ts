import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Suministro } from './suministro';

@Injectable({
  providedIn: 'root'
})
export class SuministroService {

  /* Una variable que se utiliza para almacenar la URL de la API. */
  private baseURL = "http://localhost:8080/api/v1/suministros";

  constructor(private httpClient: HttpClient) { }

  /**
   * Devuelve un Observable de una matriz de objetos Suministro
   * @returns Un Observable de una matriz de objetos Suministro.
   */
  obtenerListaDeSuministros(): Observable<Suministro[]> {
    return this.httpClient.get<Suministro[]>(`${this.baseURL}`);
  };

  /**
   * Toma como parámetro un objeto de suministro, y devuelve un observable de tipo objeto
   * @param {Suministro} suministro - Suministro: Este es el objeto que vamos a enviar al servidor.
   * @returns Un observable de tipo objeto.
   */
  registrarSuministro(suministro: Suministro): Observable<Object> {
    return this.httpClient.post(`${this.baseURL}`, suministro)
  }

  /**
   * Toma como parámetros un id y un objeto de suministro, y devuelve un Observable de tipo Object
   * @param {number} id - number - El id del suministro a actualizar.
   * @param {Suministro} suministro - Suministro: Este es el objeto que vamos a actualizar.
   * @returns Un observable de tipo objeto.
   */
  actualizarSuministro(id: number, suministro: Suministro): Observable<Object> {
    return this.httpClient.put(`${this.baseURL}/${id}`, suministro)
  }

  /**
   * Devuelve un Observable de tipo Suministro
   * @param {number} id - número
   * @returns El método devuelve un Observable de tipo Suministro.
   */
  obtenerSuministroPorId(id: number): Observable<Suministro> {
    return this.httpClient.get<Suministro>(`${this.baseURL}/${id}`);
  }

  /**
   * Toma una identificación como parámetro y devuelve un Observable de tipo Objeto
   * @param {number} id - numero: El id del suministro a ser borrado.
   * @returns el observable<Object> está siendo devuelto.
   */
  eliminarSuministro(id: number): Observable<Object> {
    return this.httpClient.delete<Suministro>(`${this.baseURL}/${id}`);
  }

}
