import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuministroDetallesComponent } from './suministro-detalles.component';

describe('SuministroDetallesComponent', () => {
  let component: SuministroDetallesComponent;
  let fixture: ComponentFixture<SuministroDetallesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuministroDetallesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuministroDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
