import { Component, OnInit } from '@angular/core';
import { Suministro } from '../suministro';
import { ActivatedRoute } from '@angular/router';
import { SuministroService } from '../suministro.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-suministro-detalles',
  templateUrl: './suministro-detalles.component.html',
  styleUrls: ['./suministro-detalles.component.css']
})
export class SuministroDetallesComponent implements OnInit {

  id: number;
  suministro: Suministro;

  constructor(private route: ActivatedRoute, private suministroServicio: SuministroService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params["id"];
    this.suministro = new Suministro();
    this.suministroServicio.obtenerSuministroPorId(this.id).subscribe(dato => {
      this.suministro = dato;
      swal(`Detalles del suministro ${this.suministro.nombre}`);
    })
  }

}
