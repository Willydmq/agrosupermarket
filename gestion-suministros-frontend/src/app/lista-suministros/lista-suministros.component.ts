import { Suministro } from '../suministro';
import { Component, OnInit } from '@angular/core';
import { SuministroService } from '../suministro.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lista-suministros',
  templateUrl: './lista-suministros.component.html',
  styleUrls: ['./lista-suministros.component.css']
})
export class ListaSuministrosComponent implements OnInit {

  suministros: Suministro[];

  constructor(private suministroServicio: SuministroService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerSuministros();
    // this.suministros = [{
    //   "id": 3,
    //   "nombre": "Lomo Cerdo",
    //   "marca": "AgroPorcinos",
    //   "registro": "2356985-NN"
    // },
    // {
    //   "id": 4,
    //   "nombre": "Atum Japon",
    //   "marca": "Xichan",
    //   "registro": "JP-8569-7"
    // }];
  }

  /**
   * La función actualizarSuministro() toma un id como parámetro y navega a la ruta
   * actualizar-suministro con el id como parámetro
   * @param {number} id - número
   */
  actualizarSuministro(id: number) {
    this.router.navigate(['actualizar-suministro', id]);
  }

  /**
   * La función toma como parámetro un id, y luego llama a la función deleteSuministro en el
   * suministroService, que toma como parámetro el id
   * @param {number} id - número
   */

  eliminarSuministro(id: number) {
    swal({
      title: '¿Estas seguro?',
      text: "Confirma si deseas eliminar el suministro",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#e8bf4c',
      cancelButtonColor: '#A06E59',
      confirmButtonText: 'Si , elimínalo',
      cancelButtonText: 'No, cancelar',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then((result) => {
      if (result.value) {
        this.suministroServicio.eliminarSuministro(id).subscribe(dato => {
          console.log(dato);
          this.obtenerSuministros();
          swal(
            'Suministro eliminado',
            'El suministro ha sido eliminado con exito',
            'success'
          )
        })
      }
    })
  }

  verDetallesdDelSuministro(id: number) {
    this.router.navigate(['suministro-detalles', id])
  }

  /**
   * La función obtenerSuministros() es una función privada que llama a la función
   * obtenerListaDeSuministros() del servicio suministroServicio, la cual devuelve un observable de tipo
   * Suministro[]. La función luego asigna el observable a la variable suministros.
   */
  private obtenerSuministros() {
    this.suministroServicio.obtenerListaDeSuministros().subscribe(dato => {
      this.suministros = dato;
    });
  }

}
