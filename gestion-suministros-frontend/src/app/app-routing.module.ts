import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaSuministrosComponent } from './lista-suministros/lista-suministros.component';
import { RegistrarSuministroComponent } from './registrar-suministro/registrar-suministro.component';
import { ActualizarSuministroComponent } from './actualizar-suministro/actualizar-suministro.component';
import { SuministroDetallesComponent } from './suministro-detalles/suministro-detalles.component';

const routes: Routes = [
  { path: 'suministros', component: ListaSuministrosComponent },
  { path: '', redirectTo: 'suministros', pathMatch: 'full' },
  { path: 'registrar-suministro', component: RegistrarSuministroComponent },
  { path: 'actualizar-suministro/:id', component: ActualizarSuministroComponent },
  { path: 'suministro-detalles/:id', component: SuministroDetallesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
