import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaSuministrosComponent } from './lista-suministros/lista-suministros.component';
import { HttpClientModule } from "@angular/common/http";
import { RegistrarSuministroComponent } from './registrar-suministro/registrar-suministro.component';
import { FormsModule } from '@angular/forms';
import { ActualizarSuministroComponent } from './actualizar-suministro/actualizar-suministro.component';
import { SuministroDetallesComponent } from './suministro-detalles/suministro-detalles.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaSuministrosComponent,
    RegistrarSuministroComponent,
    ActualizarSuministroComponent,
    SuministroDetallesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
