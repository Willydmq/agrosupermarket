package com.Agro.Supermarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgroSupermarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgroSupermarketApplication.class, args);
	}

}
