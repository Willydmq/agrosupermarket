package com.Agro.Supermarket.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Agro.Supermarket.modelo.Suministros;

@Repository
public interface SuministroRepositorio extends JpaRepository<Suministros, Long> {

}
