package com.Agro.Supermarket.controlador;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Agro.Supermarket.excepciones.ResourceNotFoundException;
import com.Agro.Supermarket.modelo.Suministros;
import com.Agro.Supermarket.repositorio.SuministroRepositorio;

@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200")
public class SuministroControlador {

	@Autowired
	private SuministroRepositorio suministroRepositorio;

	// metodo para listar todos los suministros
	@GetMapping("/suministros")
	public List<Suministros> listarTodosLosSuministros() {
		return suministroRepositorio.findAll();
	}

	// metodo para guardar los suministros
	@PostMapping("/suministros")
	public Suministros guardarSuministro(@RequestBody Suministros suministros) {
		return suministroRepositorio.save(suministros);
	}

	// este metodo sirve para buscar un suministro
	@GetMapping("/suministros/{id}")
	public ResponseEntity<Suministros> obtenerSuministroPorId(@PathVariable Long id) {
		Suministros suministros = suministroRepositorio.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(("No existe el suministro con ese ID: " + id)));
		return ResponseEntity.ok(suministros);
	}

	// este metodo sirve para actualizar suministro
	@PutMapping("/suministros/{id}")
	public ResponseEntity<Suministros> actualizarSuministro(@PathVariable Long id,
			@RequestBody Suministros detallesSuministros) {
		Suministros suministros = suministroRepositorio.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(("No existe el suministro con ese ID: " + id)));
		suministros.setNombre(detallesSuministros.getNombre());
		suministros.setMarca(detallesSuministros.getMarca());
		suministros.setRegistro(detallesSuministros.getRegistro());
		Suministros suministroActualizado = suministroRepositorio.save(suministros);
		return ResponseEntity.ok(suministroActualizado);
	}

	// este metodo sirve para eliminar un suministro
	@DeleteMapping("/suministros/{id}")
	public ResponseEntity<Map<String, Boolean>> eliminarSuministro(@PathVariable Long id) {
		Suministros suministros = suministroRepositorio.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No existe el empleado con el ID : " + id));

		suministroRepositorio.delete(suministros);
		Map<String, Boolean> respuesta = new HashMap<>();
		respuesta.put("eliminar", Boolean.TRUE);
		return ResponseEntity.ok(respuesta);
	}

}
