-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3310
-- Tiempo de generación: 21-10-2022 a las 02:49:53
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agrosupermarket`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agrosupermarket`
--

CREATE TABLE `agrosupermarket` (
  `id` bigint(20) NOT NULL,
  `marca` varchar(60) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `registro` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `agrosupermarket`
--

INSERT INTO `agrosupermarket` (`id`, `marca`, `nombre`, `registro`) VALUES
(1, 'PavoChile', 'PechugaS', '789584-CH'),
(2, 'CarnesUSA', 'LomoXX', '956896-JJ'),
(3, 'PorkHIde', 'ChuletaCC', '125689-PP'),
(4, 'FishCad', 'FileteAhu', 'IOPUO-89P'),
(5, 'FishJapan', 'PulpoMang', 'UIOI-89JP');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agrosupermarket`
--
ALTER TABLE `agrosupermarket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_29juacud7nxncigcyxbv4crpc` (`registro`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agrosupermarket`
--
ALTER TABLE `agrosupermarket`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
